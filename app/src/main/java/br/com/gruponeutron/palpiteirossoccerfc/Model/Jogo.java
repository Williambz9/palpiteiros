package br.com.gruponeutron.palpiteirossoccerfc.Model;

/**
 * Created by william on 30/01/18.
 */

public class Jogo {
    String casa, fora, hora, fase, campeonato, imcasa, imfora;


    public Jogo() {

    }

    public Jogo(String casa, String fora, String hora, String fase, String campeonato, String imcasa, String imfora) {
        this.casa = casa;
        this.fora = fora;
        this.hora = hora;
        this.campeonato = campeonato;
        this.fase = fase;
        this.imcasa = imcasa;
        this.imfora = imfora;
    }

    public String getCampeonato() {
        return campeonato;
    }

    public String getFase() {
        return fase;
    }

    public String getImcasa() {
        return imcasa;
    }

    public String getImfora() {
        return imfora;
    }

    public String getCasa() {
        return casa;
    }

    public String getFora() {
        return fora;
    }

    public String getHora() {
        return hora;
    }
}
