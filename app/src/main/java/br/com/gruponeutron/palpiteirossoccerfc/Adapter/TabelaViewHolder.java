package br.com.gruponeutron.palpiteirossoccerfc.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.gruponeutron.palpiteirossoccerfc.Callback.ItemClickListener;
import br.com.gruponeutron.palpiteirossoccerfc.R;

/**
 * Created by william on 29/12/17.
 */

public class TabelaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView time, posi, pontos, rodada;
    private ItemClickListener itemClickListener;
     public LinearLayout back;

    public TabelaViewHolder(View itemView) {
        super(itemView);
        time = (TextView) itemView.findViewById(R.id.time);
        posi = (TextView) itemView.findViewById(R.id.pos);
        pontos = (TextView) itemView.findViewById(R.id.pts);
        rodada = (TextView) itemView.findViewById(R.id.ptsRodada);
        back = (LinearLayout) itemView.findViewById(R.id.back);


    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
