package br.com.gruponeutron.palpiteirossoccerfc.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.gruponeutron.palpiteirossoccerfc.View.config.Configuracoes;
import br.com.gruponeutron.palpiteirossoccerfc.View.config.conf_conta;


public class CustomConfigPageAdapter extends FragmentPagerAdapter {

    private static final String TAG = CustomConfigPageAdapter.class.getSimpleName();

    private static final int FRAGMENT_COUNT = 2;

    public CustomConfigPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new conf_conta();

            case 1:
                return new Configuracoes();


        }
        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Conta";
            case 1:
                return "Sobre";


        }
        return null;
    }
}
