package br.com.gruponeutron.palpiteirossoccerfc.View.config;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.microedition.khronos.opengles.GL;

import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Mascara.Mask;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Atualizar;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Usuario;
import br.com.gruponeutron.palpiteirossoccerfc.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class conf_conta extends Fragment implements View.OnClickListener {


    public conf_conta() {
        // Required empty public constructor
    }

    AutoCompleteTextView nome, mEmailView, telefone;
    FirebaseDatabase database;
    DatabaseReference usuario;

    Button atua, siar;
    LinearLayout linearLayout;
    ProgressBar progressBar;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.f_conf_conta, container, false);
        nome = (AutoCompleteTextView) view.findViewById(R.id.nome);
        mEmailView = (AutoCompleteTextView) view.findViewById(R.id.email);
        telefone = (AutoCompleteTextView) view.findViewById(R.id.telefone);
        atua = (Button) view.findViewById(R.id.cadastro);
        atua.setOnClickListener(this);
        telefone.addTextChangedListener(Mask.insert("(##)#########", telefone));
        linearLayout = (LinearLayout) view.findViewById(R.id.fundo);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        textView = (TextView) view.findViewById(R.id.carre);


        siar = (Button) view.findViewById(R.id.sair);
        siar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linearLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                getActivity().finish();
                SavePreferences("login", "0");
                SavePreferences("nome", null);
                SavePreferences("senha", null);
            }
        });


        database = FirebaseDatabase.getInstance();
        usuario = database.getReference("usuarios");


        nome.setText(Global.usuarioAtual.getNome());
        mEmailView.setText(Global.usuarioAtual.getEmail());
        telefone.setText(Global.usuarioAtual.getTelefone());

        return view;
    }

    @Override
    public void onClick(View view) {

        linearLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
        usuario.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Usuario at = new Usuario(
                        nome.getText().toString(),
                        Global.usuarioAtual.getNomeusuario(),
                        mEmailView.getText().toString(),
                        telefone.getText().toString(),
                        Global.usuarioAtual.getNomedotime(),
                        Global.usuarioAtual.getCordotime(),
                        Global.usuarioAtual.getSenha(),
                        Global.usuarioAtual.getCordotime2(),
                        Global.usuarioAtual.getEmblema(),
                        Global.usuarioAtual.getId());
                usuario.child(Global.usuarioAtual.getNomeusuario()).setValue(at);


                linearLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                textView.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Toast.makeText(getActivity(), "Erro ao atualizar os dados", Toast.LENGTH_LONG).show();

                linearLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                textView.setVisibility(View.GONE);
            }
        });


    }

    private void SavePreferences(String key, String value) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
