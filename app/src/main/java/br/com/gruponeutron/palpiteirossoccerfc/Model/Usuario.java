package br.com.gruponeutron.palpiteirossoccerfc.Model;

/**
 * Created by william on 29/01/18.
 */

 public class Usuario {

    private String nome;
    private String nomeusuario;
    private String email;
    private String telefone;
    private String nomedotime;
    private String cordotime;
    private String senha;
    private String emblema;
    private String cordotime2;
    private int id;

    public Usuario(){

    }

    public  Usuario(String nome,
                   String nomeusuario,
                   String email,
                   String telefone,
                   String nomedotime,
                   String cordotime,
                   String senha,
                   String cordotime2,
                   String emblema,
                   int id){

        this.id = id;
        this.nome = nome;
        this.nomeusuario = nomeusuario;
        this.email = email;
        this.telefone = telefone;
        this.senha = senha;
        this.nomedotime = nomedotime;
        this.cordotime = cordotime;
        this.cordotime2 = cordotime2;
        this.emblema = emblema;



    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeusuario() {
        return nomeusuario;
    }

    public void setNomeusuario(String nomeusuario) {
        this.nomeusuario = nomeusuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNomedotime() {
        return nomedotime;
    }

    public void setNomedotime(String nomedotime) {
        this.nomedotime = nomedotime;
    }

    public String getCordotime() {
        return cordotime;
    }

    public void setCordotime(String cordotime) {
        this.cordotime = cordotime;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCordotime2() {
        return cordotime2;
    }

    public void setCordotime2(String cordotime2) {
        this.cordotime2 = cordotime2;
    }

    public String getEmblema() {
        return emblema;
    }

    public void setEmblema(String emblema) {
        this.emblema = emblema;
    }
}
