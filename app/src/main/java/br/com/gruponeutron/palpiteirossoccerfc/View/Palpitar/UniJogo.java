package br.com.gruponeutron.palpiteirossoccerfc.View.Palpitar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Jogo;
import br.com.gruponeutron.palpiteirossoccerfc.Model.PalpiteJogos;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Usuario;
import br.com.gruponeutron.palpiteirossoccerfc.R;

import static android.widget.Toast.LENGTH_LONG;

public class UniJogo extends AppCompatActivity implements View.OnClickListener {

    TextView casa, fora, ccasa, cfora, hora, classico, finl, carre;
    EditText edtcasa, edtfora;
    Button enviar;
    ProgressBar progressBar;
    CardView cardView;
    ImageView imcasa, imfora;


    FirebaseDatabase database;
    DatabaseReference jogos, palpenviados, dias;
    int idatual = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_uni_jogo);


        database = FirebaseDatabase.getInstance();
        jogos = database.getReference("Palpites");
        palpenviados = database.getReference("palpEnviados");
        dias = database.getReference("global");

        casa = (TextView) findViewById(R.id.casa);
        fora = (TextView) findViewById(R.id.fora);
        hora = (TextView) findViewById(R.id.hora);
        classico = (TextView) findViewById(R.id.classic);
        finl = (TextView) findViewById(R.id.fina);
        ccasa = (TextView) findViewById(R.id.casacompleto);
        cfora = (TextView) findViewById(R.id.foracompleto);
        edtcasa = (EditText) findViewById(R.id.palpcasa);
        edtfora = (EditText) findViewById(R.id.palpfora);
        enviar = (Button) findViewById(R.id.palpitar);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        carre = (TextView) findViewById(R.id.carre);
        cardView = (CardView) findViewById(R.id.card);
        imcasa = (ImageView) findViewById(R.id.imcasa);
        imfora = (ImageView)findViewById(R.id.imfora);


        enviar.setOnClickListener(this);
        dados();

    }

    @Override
    public void onClick(View view) {

        String valCasa = edtcasa.getText().toString();
        String valFora = edtfora.getText().toString();
        String time = Global.usuarioAtual.getNomedotime();


        if (valCasa.isEmpty() || valFora.isEmpty()) {
            Toast.makeText(UniJogo.this, "Há dados em branco", LENGTH_LONG).show();
        } else {

            SimpleDateFormat dateFormat_hora = new SimpleDateFormat("HH:mm:ss");
            Date data = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(data);
            Date data_atual = cal.getTime();
            String hora_atual = dateFormat_hora.format(data_atual);

            Double temp = Double.parseDouble(hora.getText().toString().substring(0, 2));
            Double hor = Double.parseDouble(hora.getText().toString().substring(3, 5));
            Double tempatul = Double.parseDouble(hora_atual.substring(0, 2));
            Double horatual = Double.parseDouble(hora_atual.substring(3, 5));

            Double c = Double.parseDouble(valCasa);
            Double f = Double.parseDouble(valFora);
            String jogo = null;
            if (c > f) {
                jogo = "casa";
            }
            if (c.equals(f)) {
                jogo = "empate";
            }
            if (c < f) {
                jogo = "fora";
            }


            if (temp < tempatul) {
                Toast.makeText(UniJogo.this, "Horario ja ultrapassou, portanto seu palpite é invalido.", LENGTH_LONG).show();
                idatual = idatual + 1;
                dados();
                edtfora.setText("");
                edtcasa.setText("");
                cardView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                carre.setVisibility(View.VISIBLE);
            } else if (temp == tempatul) {
                if (hor < horatual) {
                    idatual = idatual + 1;
                    dados();
                    edtfora.setText("");
                    edtcasa.setText("");
                    cardView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    carre.setVisibility(View.VISIBLE);
                    Toast.makeText(UniJogo.this, "Horario ja ultrapassou", LENGTH_LONG).show();
                } else {
                    PalpiteJogos plt = new PalpiteJogos(valCasa, valFora, jogo);
                    palpenviados.child(Global.usuarioAtual.getNomedotime()).child("Jogo" + String.valueOf(idatual)).setValue(plt);

                    idatual = idatual + 1;
                    dados();
                    edtfora.setText("");
                    edtcasa.setText("");
                    cardView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    carre.setVisibility(View.VISIBLE);

                }
            } else {
                PalpiteJogos plt = new PalpiteJogos(valCasa, valFora, jogo);
                palpenviados.child(time).child("Jogo" + String.valueOf(idatual)).setValue(plt);

                idatual = idatual + 1;
                dados();
                edtfora.setText("");
                edtcasa.setText("");
                cardView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                carre.setVisibility(View.VISIBLE);

            }


        }


    }

    public void dados() {

        dias.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final String idx = String.valueOf(dataSnapshot.child("qtdJogos").getValue());
                final Double idmax = Double.parseDouble(idx);

                jogos.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String val = "Jogo" + String.valueOf(idatual);

                        if (idatual < idmax) {

                            Jogo model = (dataSnapshot.child(val).getValue(Jogo.class));


                            Picasso.get().load(model.getImcasa()).into(imcasa);
                            Picasso.get().load(model.getImfora()).into(imfora);
                            if (model.getFase().equals("grup")){
                                finl.setText("FASE DE GRUPOS");
                            } else if (model.getFase().equals("oita")){
                                finl.setText("OITAVAS DE FINAL");
                            } else if (model.getFase().equals("quar")){
                                finl.setText("QUARTAS DE FINAL");
                            } else if (model.getFase().equals("semi")){
                                finl.setText("SEMI FINAL");
                            } else if (model.getFase().equals("terc")){
                                finl.setText("TERCEIRO LUGAR");
                            } else if (model.getFase().equals("fina")){
                                finl.setText("FINALLL");
                            }
                            hora.setText((model.getHora()));
                            fora.setText(String.valueOf(model.getFora().substring(0, 3)));
                            casa.setText(String.valueOf(model.getCasa().substring(0, 3)));
                            cfora.setText(model.getFora());
                            ccasa.setText(model.getCasa());
                            finl.setText(model.getFase());


                            cardView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            carre.setVisibility(View.GONE);


                        } else if (idatual == idmax) {



                            Jogo model = (dataSnapshot.child(val).getValue(Jogo.class));


                            Picasso.get().load(model.getImcasa()).into(imcasa);
                            Picasso.get().load(model.getImfora()).into(imfora);
                            if (model.getFase().equals("grup")){
                                finl.setText("FASE DE GRUPOS");
                            } else if (model.getFase().equals("oita")){
                                finl.setText("OITAVAS DE FINAL");
                            } else if (model.getFase().equals("quar")){
                                finl.setText("QUARTAS DE FINAL");
                            } else if (model.getFase().equals("semi")){
                                finl.setText("SEMI FINAL");
                            } else if (model.getFase().equals("terc")){
                                finl.setText("TERCEIRO LUGAR");
                            } else if (model.getFase().equals("fina")){
                                finl.setText("FINALLL");
                            }
                            hora.setText((model.getHora()));
                            fora.setText(String.valueOf(model.getFora().substring(0, 3)));
                            casa.setText(String.valueOf(model.getCasa().substring(0, 3)));
                            cfora.setText(model.getFora());
                            ccasa.setText(model.getCasa());
                            finl.setText(model.getFase());
                            enviar.setText("Concluir");
                            cardView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            carre.setVisibility(View.GONE);

                        } else {
                            carre.setText("Seus Palpites Foram Enviados...");
                            progressBar.setVisibility(View.GONE);
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
