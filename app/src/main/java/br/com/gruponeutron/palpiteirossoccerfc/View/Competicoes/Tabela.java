package br.com.gruponeutron.palpiteirossoccerfc.View.Competicoes;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.gruponeutron.palpiteirossoccerfc.Adapter.JogosViewHolder;
import br.com.gruponeutron.palpiteirossoccerfc.Adapter.TabelaViewHolder;
import br.com.gruponeutron.palpiteirossoccerfc.Callback.ItemClickListener;
import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Model.ComptRanking;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Jogo;
import br.com.gruponeutron.palpiteirossoccerfc.R;

public class Tabela extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference competicoes;
    FirebaseRecyclerAdapter<ComptRanking, TabelaViewHolder> adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ProgressBar progressBar;
    TextView textView, erro;
    LinearLayout top;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabela);
        textView = (TextView) findViewById(R.id.carre);
        erro = (TextView) findViewById(R.id.conexao);
        top = (LinearLayout) findViewById(R.id.lay);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        Intent intent = getIntent();
        Bundle dados = intent.getExtras();

        String serie = dados.getString("liga");



        recyclerView = (RecyclerView) findViewById(R.id.tabela);
        database = FirebaseDatabase.getInstance();
        competicoes = database.getReference("Ligas/" + serie);


        competicoes.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    textView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    top.setVisibility(View.VISIBLE);

                } else {
                    textView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    erro.setVisibility(View.VISIBLE);
                    erro.setText("Essa a liga não possui dados para serem apresentados...");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                textView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                erro.setVisibility(View.VISIBLE);
            }
        });


        layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        layoutManager.setReverseLayout(true);

        recyclerView.setLayoutManager(layoutManager);


        getSupportActionBar().setTitle("Liga Palpiteco");


        adapter = new FirebaseRecyclerAdapter<ComptRanking, TabelaViewHolder>(
                ComptRanking.class,
                R.layout.itemtabela,
                TabelaViewHolder.class,
                competicoes.orderByChild("ptsrodada")
        ) {

            @Override
            protected void populateViewHolder(TabelaViewHolder viewHolder, ComptRanking model, int position) {


                viewHolder.time.setText(String.valueOf(model.getTime()));
                viewHolder.pontos.setText(String.valueOf(model.getPontos()));
                viewHolder.rodada.setText(String.valueOf(model.getRodada()));
                viewHolder.posi.setText("");



            }


        };


        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionsair:

                startActivity(new Intent(Tabela.this, Informacoes.class));
                // Action goes here
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
