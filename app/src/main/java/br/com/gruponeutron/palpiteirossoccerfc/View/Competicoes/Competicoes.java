package br.com.gruponeutron.palpiteirossoccerfc.View.Competicoes;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.gruponeutron.palpiteirossoccerfc.Adapter.TabelaViewHolder;
import br.com.gruponeutron.palpiteirossoccerfc.Model.ComptRanking;
import br.com.gruponeutron.palpiteirossoccerfc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Competicoes extends Fragment {


    public Competicoes() {
        // Required empty public constructor
    }
    CardView lig1, lig2, lig3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.f_competicoes, container, false);

        lig1 = (CardView) view.findViewById(R.id.serieA);
        lig2 = (CardView) view.findViewById(R.id.serieB);
        lig3 = (CardView) view.findViewById(R.id.serieC);

        lig1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Tabela.class );
                t.putExtra("liga", "Liga palpiteiros");
                startActivity(t);
            }
        });
        lig2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Tabela.class );
                t.putExtra("Serie", "B");
                startActivity(t);
            }
        });    lig3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Tabela.class );
                t.putExtra("Serie", "C");
                startActivity(t);
            }
        });





   return view;
    }

}
