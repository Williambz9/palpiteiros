package br.com.gruponeutron.palpiteirossoccerfc.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.gruponeutron.palpiteirossoccerfc.R;

/**
 * Created by william on 08/02/18.
 */


public class AdapterSpinner extends  BaseAdapter {
    Context context;
    Integer[] cor;
    String[] countryNames;
    LayoutInflater inflter;

    public AdapterSpinner(Context applicationContext, Integer[] cor, String[] countryNames) {
        this.context = applicationContext;
        this.cor = cor;
        this.countryNames = countryNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return cor.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.itemspinner, null);


        TextView ico = (TextView) view.findViewById(R.id.imgLanguage);
        TextView names = (TextView) view.findViewById(R.id.tvLanguage);
        names.setText(countryNames[i]);
        ico.setBackgroundColor(cor[i]);
        return view;
    }
}