package br.com.gruponeutron.palpiteirossoccerfc.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.gruponeutron.palpiteirossoccerfc.Callback.ItemClickListener;
import br.com.gruponeutron.palpiteirossoccerfc.R;

/**
 * Created by william on 29/12/17.
 */

public class JogosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView casa, fora, hora, classico, fin, compCasa, comFora;
    public ImageView imcasa, imfora;
    public EditText casP, foraaP;
    private ItemClickListener itemClickListener;

    public JogosViewHolder(View itemView) {
        super(itemView);
        casa = (TextView) itemView.findViewById(R.id.casa);
        fora = (TextView) itemView.findViewById(R.id.fora);
        hora = (TextView) itemView.findViewById(R.id.hora);
        classico = (TextView) itemView.findViewById(R.id.classic);
        fin = (TextView) itemView.findViewById(R.id.fina);
        compCasa = (TextView) itemView.findViewById(R.id.casacompleto);
        comFora = (TextView) itemView.findViewById(R.id.foracompleto);
        casP = (EditText) itemView.findViewById(R.id.palpcasa);
        foraaP = (EditText) itemView.findViewById(R.id.palpfora);
        imcasa = (ImageView) itemView.findViewById(R.id.imcasa);
        imfora = (ImageView) itemView.findViewById(R.id.imfora);


        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
