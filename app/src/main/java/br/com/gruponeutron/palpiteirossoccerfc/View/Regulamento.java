package br.com.gruponeutron.palpiteirossoccerfc.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import br.com.gruponeutron.palpiteirossoccerfc.R;

public class Regulamento extends AppCompatActivity {


    CheckBox checkBox;
    Button avancar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_regulamento);
        avancar = (Button) findViewById(R.id.avancar);
        checkBox = (CheckBox) findViewById(R.id.checkregualmento);

        avancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked()){
                    startActivity(new Intent(Regulamento.this, Escolher_ligas.class));
                    finish();
                     }
            }
        });
    }

    private void SavePreferences(String key, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
