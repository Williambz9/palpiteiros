package br.com.gruponeutron.palpiteirossoccerfc.View.config;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import br.com.gruponeutron.palpiteirossoccerfc.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Configuracoes extends Fragment {


    public Configuracoes() {
        // Required empty public constructor
    }

    Switch resul, jogo, taca;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.config_gerais, container, false);

        resul = (Switch) view.findViewById(R.id.resul);
        jogo = (Switch) view.findViewById(R.id.jogos);
        taca = (Switch) view.findViewById(R.id.taca);

        resul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swit();
            }
        });
        jogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swit();
            }
        });
        taca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swit();
            }
        });


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        String re = sharedPreferences.getString("resultado", "");
        final String jo = sharedPreferences.getString("jogo", "");
        final String ta = sharedPreferences.getString("taca", "");

        if (re.equals("1")) {
            resul.setChecked(true);
        }
        if (jo.equals("1")) {
            jogo.setChecked(true);
        }
        if (ta.equals("1")) {
            taca.setChecked(true);
        }


        return view;
    }

    public void swit() {
        Boolean state = resul.isChecked();
        Boolean state1 = taca.isChecked();
        Boolean state2 =jogo.isChecked();
        if (state == true) {
            SavePreferences("resultado", "1");
        } else {
            SavePreferences("resultado", "0");
        }
        if (state1 == true) {
            SavePreferences("taca", "1");
        } else {
            SavePreferences("taca", "0");
        }if (state2 == true) {
            SavePreferences("jogo", "1");
        } else {
            SavePreferences("jogo", "0");
        }




    }

    private void SavePreferences(String key, String value) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
