package br.com.gruponeutron.palpiteirossoccerfc.View.Menu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import br.com.gruponeutron.palpiteirossoccerfc.R;

public class Noticia extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticia);
        WebView webView = (WebView) findViewById(R.id.web);

        Intent intent = getIntent();
        Bundle dados = intent.getExtras();
        getSupportActionBar().setElevation(0);

        String serie = dados.getString("link");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started
                // Visible the progressbar
            //    mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url){
                // Do something when page loading finished
            }
        });

        webView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view, int newProgress){
                // Update the progress bar with page loading progress
            //    mProgressBar.setProgress(newProgress);
                if(newProgress == 100){
                    // Hide the progressbar
              //      mProgressBar.setVisibility(View.GONE);
                }
            }
        });

        // Enable the javascript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://www.listalocall.com/");



        webView.loadUrl(serie);
    }
}
