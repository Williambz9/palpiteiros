package br.com.gruponeutron.palpiteirossoccerfc.View.RankingClubes;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.gruponeutron.palpiteirossoccerfc.Adapter.JogosViewHolder;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Jogo;
import br.com.gruponeutron.palpiteirossoccerfc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RankingClub extends Fragment {


    public RankingClub() {
        // Required empty public constructor
    }

    RecyclerView lista;

    FirebaseRecyclerAdapter<Jogo, JogosViewHolder> adapter;
    LinearLayoutManager layoutManager;
    FirebaseDatabase database;
    DatabaseReference ranking;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.f_ranking_club, container, false);


        lista = (RecyclerView) view.findViewById(R.id.ranking);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);
        final TextView carre = (TextView) view.findViewById(R.id.carre);
        final TextView semconx = (TextView) view.findViewById(R.id.conexao);


        database = FirebaseDatabase.getInstance();
        ranking = database.getReference("Ranking");

        ranking.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    progressBar.setVisibility(View.GONE);
                    carre.setVisibility(View.GONE);
                    lista.setVisibility(View.VISIBLE);
                }else {
                    progressBar.setVisibility(View.GONE);
                    carre.setVisibility(View.GONE);
                    semconx.setVisibility(View.VISIBLE);
                    semconx.setText("Ainda não há dados para o ranking...");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);
                carre.setVisibility(View.GONE);
                semconx.setVisibility(View.VISIBLE);
                semconx.setText("Erro ao conectar-se...");
            }
        });


        return view;
    }

}
