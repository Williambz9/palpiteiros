package br.com.gruponeutron.palpiteirossoccerfc.View.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Comment;

import br.com.gruponeutron.palpiteirossoccerfc.Adapter.AdapterSpinner;
import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Mascara.Mask;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Ligass;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Usuario;
import br.com.gruponeutron.palpiteirossoccerfc.Model.id;
import br.com.gruponeutron.palpiteirossoccerfc.R;
import br.com.gruponeutron.palpiteirossoccerfc.View.Regulamento;


public class CadastroActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    AutoCompleteTextView nome, nomeuser, mEmailView, telefone, nomedotime;
    Button cadastro;
    EditText mPasswordView, confirsenha;
    FirebaseDatabase database;
    DatabaseReference usuario, times, ligas, global;
    int avatar;
    TextView cadas;
    ProgressBar progressBar;
    Spinner cor, cor2;
    LinearLayout fundo;
    String c1, c2;
    String[] cores = {"Azul", "Verde", "Amarelo", "Preto", "Vermelho", "Branco"};
    Integer[] corz = {Color.BLUE, Color.parseColor("#32B846"), Color.parseColor("#ffd700"),
            Color.BLACK, Color.RED, Color.WHITE};

    RadioButton r1, r2, r3, r4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_cadastro);
        nome = (AutoCompleteTextView) findViewById(R.id.nome);
        nomeuser = (AutoCompleteTextView) findViewById(R.id.nomeuser);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        telefone = (AutoCompleteTextView) findViewById(R.id.telefone);
        nomedotime = (AutoCompleteTextView) findViewById(R.id.time);
        r1 = (RadioButton) findViewById(R.id.rd1);
        r2 = (RadioButton) findViewById(R.id.rd2);
        r3 = (RadioButton) findViewById(R.id.rd3);
        r4 = (RadioButton) findViewById(R.id.rd4);

        telefone.addTextChangedListener(Mask.insert("(##)#########", telefone));


        mPasswordView = (EditText) findViewById(R.id.password);
        confirsenha = (EditText) findViewById(R.id.confirpassword);
        cadastro = (Button) findViewById(R.id.confirm);
        cadastro.setOnClickListener(this);
        fundo = (LinearLayout) findViewById(R.id.fundo);
        cadas = (TextView) findViewById(R.id.textcadast);
        database = FirebaseDatabase.getInstance();
        usuario = database.getReference("usuarios");
        times = database.getReference("Times");

        global = database.getReference("global");


        ligas = database.getReference("Ligas");
        progressBar = (ProgressBar) findViewById(R.id.progress);
        cor = (Spinner) findViewById(R.id.cor);
        cor2 = (Spinner) findViewById(R.id.cor2);
        getSupportActionBar().hide();
        cor2.setOnItemSelectedListener(this);


        final AdapterSpinner adapterSpinner = new AdapterSpinner(getApplicationContext(), corz, cores);
        cor.setAdapter(adapterSpinner);
        cor2.setAdapter(adapterSpinner);

        cor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                c1 = cores[i];
                //       Toast.makeText(CadastroActivity.this, c1, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        cor2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                c2 = cores[i];
                Toast.makeText(CadastroActivity.this, c1, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onClick(View view) {

        Toast.makeText(CadastroActivity.this, c1 + c2, Toast.LENGTH_SHORT).show();

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        nome.setError(null);
        nomeuser.setError(null);
        confirsenha.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String name = nome.getText().toString();
        final String nameuser = nomeuser.getText().toString();
        String confir = confirsenha.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.

        if (TextUtils.isEmpty(name)) {
            nome.setError(getString(R.string.error_field_required));
            focusView = nome;
            cancel = true;
        }

        if (TextUtils.isEmpty(nameuser)) {
            nomeuser.setError(getString(R.string.error_field_required));
            focusView = nomeuser;
            cancel = true;
        }
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(confir)) {
            confirsenha.setError(getString(R.string.error_field_required));
            focusView = confirsenha;
            cancel = true;
        }
        if (!isPasswordequals(password, confir)) {
            mPasswordView.setError(("Senha não confere"));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailVali(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {

            focusView.requestFocus();
        } else if (!r1.isChecked() && !r2.isChecked() && !r3.isChecked() && !r4.isChecked()) {
            Toast.makeText(CadastroActivity.this, "Selecione o escudo man", Toast.LENGTH_SHORT).show();
        } else {


            fundo.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            cadas.setVisibility(View.VISIBLE);
            cadastro.setVisibility(View.GONE);


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    times.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.child(nomedotime.getText().toString()).exists()) {
                                boolean cancel = false;
                                View focusView = null;

                                fundo.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                cadas.setVisibility(View.GONE);
                                nomedotime.setError("Esse time ja é utilizado por outro usuário!");
                                focusView = mEmailView;
                                cancel = true;

                            } else {


                                global.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String ia = String.valueOf(dataSnapshot.child("UserId").getValue());
                                        Double ie = 1 + Double.parseDouble(ia);
                                        int id = ie.intValue();
                                        String liga = "Copa Palpiteiros";

                                        //    if (ie < 21) {liga = "Série A";                                        }

                                        //    if (ie < 41 && ie > 20) { liga = "Série B";}
                                        //    if (ie > 40) {liga = "Série C";}


                                        final Ligass ligass = new Ligass(nomedotime.getText().toString(),
                                                0,
                                                0, nomeuser.getText().toString());
                                        String emblema = null;
                                        if (r1.isChecked()) {
                                            emblema = "emb1";
                                        } else if (r2.isChecked()) {
                                            emblema = "emb2";
                                        } else if (r3.isChecked()) {
                                            emblema = "emb3";
                                        } else if (r4.isChecked()) {
                                            emblema = "emb4";
                                        } else {
                                            emblema = "emb1";
                                        }


                                        final Usuario user = new Usuario(nome.getText().toString(),
                                                nomeuser.getText().toString(),
                                                mEmailView.getText().toString(),
                                                telefone.getText().toString(),
                                                nomedotime.getText().toString(),
                                                c1,
                                                mPasswordView.getText().toString(),
                                                c2, emblema, id);


                                        if (dataSnapshot.child(nomeuser.getText().toString()).exists()) {
                                            Toast.makeText(CadastroActivity.this, "Usuário já existe", Toast.LENGTH_LONG).show();
                                            fundo.setVisibility(View.VISIBLE);
                                            progressBar.setVisibility(View.GONE);
                                            cadas.setVisibility(View.GONE);


                                        } else {
                                            usuario.child(user.getNomeusuario()).setValue(user);
                                            times.child(String.valueOf(id)).child("nome").setValue(user.getNomeusuario());
                                            global.child("UserId").setValue(id);

                                            Toast.makeText(CadastroActivity.this, "Cadastro Efetuado", Toast.LENGTH_LONG).show();
                                            Global.usuarioAtual = user;
                                            startActivity(new Intent(CadastroActivity.this, Regulamento.class));
                                            finish();

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(CadastroActivity.this, "Erro", Toast.LENGTH_LONG).show();

                                    }
                                });


                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(CadastroActivity.this, "Erro", Toast.LENGTH_LONG).show();

                        }
                    });


                }
            }, 1000);


        }

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private boolean isEmailVali(String email) {
        //TODO: Replace this with your own logic
        return email.contains(".com");
    }

    private boolean isPasswordequals(String password, String confPass) {
        //TODO: Replace this with your own logic
        return password.equals(confPass);
    }


}
