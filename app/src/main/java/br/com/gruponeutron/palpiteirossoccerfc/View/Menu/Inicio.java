package br.com.gruponeutron.palpiteirossoccerfc.View.Menu;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.R;
import br.com.gruponeutron.palpiteirossoccerfc.View.Regulamento;

/**
 * A simple {@link Fragment} subclass.
 */
public class Inicio extends Fragment {


    public Inicio() {
        // Required empty public constructor
    }

    FirebaseDatabase database;
    TextView cor;
    TextView cor2;
    ImageView imageView;
    CardView share, regul;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_inicio, container, false);
        TextView time = (TextView) view.findViewById(R.id.usertime);
        time.setText(Global.usuarioAtual.getNomedotime());
        final TextView name = (TextView) view.findViewById(R.id.username);
        name.setText(Global.usuarioAtual.getNomeusuario());
        final CardView bemv = (CardView) view.findViewById(R.id.bemvindo);
        regul = (CardView) view.findViewById(R.id.regulamento);
        regul = (CardView) view.findViewById(R.id.regulamento);
        regul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Regulamento.class));
            }
        });

        CardView placar = (CardView) view.findViewById(R.id.placar);
        placar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Noticia.class);
                t.putExtra("link", "http://globoesporte.globo.com/placar-ge/hoje/");
                startActivity(t);
            }
        });


        share = (CardView) view.findViewById(R.id.share);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent compartilha = new Intent(Intent.ACTION_SEND);
                compartilha.setType("text/plain");
                compartilha.putExtra(Intent.EXTRA_SUBJECT, "");
                compartilha.putExtra(Intent.EXTRA_TEXT, "  Jogue também o Palpiteiros Soccer FC! www.google.com!");
                startActivity(Intent.createChooser(compartilha, "Convidar amigos"));

            }
        });

        cor = (TextView) view.findViewById(R.id.cor);
        cor2 = (TextView) view.findViewById(R.id.cor2);
        imageView = (ImageView) view.findViewById(R.id.trofeu);

        database = FirebaseDatabase.getInstance();
        cor();


        return view;
    }


    public void cor() {

        String emb = Global.usuarioAtual.getEmblema();

        if (emb.equals("emb1")) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.escudo1));
        } else if (emb.equals("emb2")) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.escudo2));
        } else if (emb.equals("emb3")) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.escudo3));
        } else if (emb.equals("emb4")) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.escudo4));
        }

        String cor1 = Global.usuarioAtual.getCordotime();
        String cow2 = Global.usuarioAtual.getCordotime2();

        if (cor1.equals("Azul")) {
            cor.setBackgroundColor(Color.BLUE);
        }
        if (cor1.equals("Verde")) {
            cor.setBackgroundColor(Color.parseColor("#32B846"));
        }
        if (cor1.equals("Amarelo")) {
            cor.setBackgroundColor(Color.YELLOW);
        }
        if (cor1.equals("Preto")) {
            cor.setBackgroundColor(Color.BLACK);
        }
        if (cor1.equals("Vermelho")) {
            cor.setBackgroundColor(Color.RED);
        }
        if (cor1.equals("Branco")) {
            cor.setBackgroundColor(Color.WHITE);
        }

        if (cow2.equals("Azul")) {
            cor2.setBackgroundColor(Color.BLUE);
        }
        if (cow2.equals("Verde")) {
            cor2.setBackgroundColor(Color.parseColor("#32B846"));
        }
        if (cow2.equals("Amarelo")) {
            cor2.setBackgroundColor(Color.YELLOW);
        }
        if (cow2.equals("Preto")) {
            cor2.setBackgroundColor(Color.BLACK);
        }
        if (cow2.equals("Vermelho")) {
            cor2.setBackgroundColor(Color.RED);
        }
        if (cow2.equals("Branco")) {
            cor2.setBackgroundColor(Color.WHITE);
        }


    }

}
