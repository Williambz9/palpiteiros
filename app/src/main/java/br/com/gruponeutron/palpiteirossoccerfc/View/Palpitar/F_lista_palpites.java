package br.com.gruponeutron.palpiteirossoccerfc.View.Palpitar;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import br.com.gruponeutron.palpiteirossoccerfc.Adapter.JogosViewHolder;
import br.com.gruponeutron.palpiteirossoccerfc.Callback.ItemClickListener;
import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Jogo;
import br.com.gruponeutron.palpiteirossoccerfc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class F_lista_palpites extends Fragment {


    public F_lista_palpites() {
        // Required empty public constructor
    }

    RecyclerView lista;

    FirebaseRecyclerAdapter<Jogo, JogosViewHolder> adapter;
    LinearLayoutManager layoutManager;
    FirebaseDatabase database;
    DatabaseReference jogos, ligas, palpenviados;
    Button palpitar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.a_list__palpites, container, false);
        lista = (RecyclerView) view.findViewById(R.id.list_palpites);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);
        final TextView carre = (TextView) view.findViewById(R.id.carre);
        TextView semconx = (TextView) view.findViewById(R.id.conexao);
        palpitar = (Button) view.findViewById(R.id.palpitar);
        palpitar.setVisibility(View.GONE);

        palpenviados = FirebaseDatabase.getInstance().getReference("palpEnviados");


        database = FirebaseDatabase.getInstance();
        jogos = database.getReference("Palpites");


        palpitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), UniJogo.class));


            }
        });
        layoutManager = new LinearLayoutManager(getActivity());
        lista.setHasFixedSize(true);

        lista.setLayoutManager(layoutManager);


        jogos.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    progressBar.setVisibility(View.GONE);
                    carre.setVisibility(View.GONE);
                    lista.setVisibility(View.VISIBLE);
                    carre();
                } else {
                    progressBar.setVisibility(View.GONE);

                    carre.setText("Os jogos ainda não foram publicados, aguarde...");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return view;
    }


    private void carre() {


        adapter = new FirebaseRecyclerAdapter<Jogo, JogosViewHolder>(
                Jogo.class,
                R.layout.item_jogopalpite,
                JogosViewHolder.class,
                jogos.orderByChild("hora")
        ) {

            @Override
            protected void populateViewHolder(final JogosViewHolder viewHolder, final Jogo model, final int position) {

                viewHolder.hora.setText((model.getHora()));
                viewHolder.fora.setText(String.valueOf(model.getFora().substring(0, 3)));
                viewHolder.casa.setText(String.valueOf(model.getCasa().substring(0, 3)));
                viewHolder.comFora.setText(model.getFora());
                viewHolder.compCasa.setText(model.getCasa());
                viewHolder.fin.setText(model.getFase());

                Picasso.get().load(model.getImcasa()).into(viewHolder.imcasa);
                Picasso.get().load(model.getImfora()).into(viewHolder.imfora);
                if (model.getFase().equals("grup")){
                    viewHolder.fin.setText("FASE DE GRUPOS");
                } else if (model.getFase().equals("oita")){
                    viewHolder.fin.setText("OITAVAS DE FINAL");
                } else if (model.getFase().equals("quar")){
                    viewHolder.fin.setText("QUARTAS DE FINAL");
                } else if (model.getFase().equals("semi")){
                    viewHolder.fin.setText("SEMI FINAL");
                } else if (model.getFase().equals("terc")){
                    viewHolder.fin.setText("TERCEIRO LUGAR");
                } else if (model.getFase().equals("fina")){
                    viewHolder.fin.setText("FINALLL");
                }

                Double jogo = Double.valueOf(1);


                if (!model.getFase().isEmpty()) {
                    viewHolder.classico.setVisibility(View.VISIBLE);
                }


            }
        };

        palpitou();

        adapter.notifyDataSetChanged();
        lista.setAdapter(adapter);


    }

    private void palpitou() {

        palpenviados.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.child(Global.usuarioAtual.getNomedotime()).exists()) {
                    palpitar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}
