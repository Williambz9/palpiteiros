package br.com.gruponeutron.palpiteirossoccerfc.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Ligass;
import br.com.gruponeutron.palpiteirossoccerfc.R;

public class Escolher_ligas extends AppCompatActivity {

    CheckBox principal, ligatal, ligax;

    DatabaseReference usuario, times, ligas, global;

    FirebaseDatabase database;
    Button concluir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolher_ligas);

        principal = (CheckBox) findViewById(R.id.ligaprin);
        ligatal = (CheckBox) findViewById(R.id.ligatal);
        ligax = (CheckBox) findViewById(R.id.ligatop);


        ligas = FirebaseDatabase.getInstance().getReference("Ligas");
        concluir = (Button) findViewById(R.id.concluir);


        concluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Ligass ligass = new Ligass(Global.usuarioAtual.getNomedotime(),
                        0,
                        0, Global.usuarioAtual.getNomeusuario());

                int cont = 4;

                if (principal.isChecked()) {
                    ligas.child("Liga palpiteiros").child(Global.usuarioAtual.getNomedotime()).setValue(ligass);
                    cont = cont + 1;
                }

                if (ligatal.isChecked()) {
                    ligas.child("Liga tal").child(Global.usuarioAtual.getNomedotime()).setValue(ligass);
                    cont = cont + 1;
                }

                if (ligax.isChecked()) {
                    ligas.child("Liga x").child(Global.usuarioAtual.getNomedotime()).setValue(ligass);
                    cont = cont + 1;
                }

                if (!ligax.isChecked() && !ligatal.isChecked() && !principal.isChecked()) {
                    Toast.makeText(Escolher_ligas.this, "Escolha pelo menos uma liga", Toast.LENGTH_SHORT).show();
                }

                startActivity(new Intent(Escolher_ligas.this, MenuPrincipal.class));
            }
        });


    }
}
