package br.com.gruponeutron.palpiteirossoccerfc.View;

import android.app.ActionBar;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.gruponeutron.palpiteirossoccerfc.R;
import br.com.gruponeutron.palpiteirossoccerfc.View.Competicoes.Competicoes;
import br.com.gruponeutron.palpiteirossoccerfc.View.Menu.Inicio;
import br.com.gruponeutron.palpiteirossoccerfc.View.Palpitar.F_lista_palpites;
import br.com.gruponeutron.palpiteirossoccerfc.View.RankingClubes.RankingClub;
import br.com.gruponeutron.palpiteirossoccerfc.View.config.Configuracoes;
import br.com.gruponeutron.palpiteirossoccerfc.View.config.LibraryFragment;

public class MenuPrincipal extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportActionBar().hide();
                    getSupportActionBar().setTitle("Palpiteiros Soccer FC");
                    Inicio fragmenttab = new Inicio();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.contentContainer, fragmenttab).commit();
                    return true;
                case R.id.palpites:
                    getSupportActionBar().setTitle("Palpitar");
                    F_lista_palpites fpalpitar = new F_lista_palpites();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.contentContainer, fpalpitar).commit();
                    return true;
                case R.id.competicoes:
                    getSupportActionBar().setTitle("Competições");

                    Competicoes fpa = new Competicoes();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.contentContainer, fpa).commit();
                    return true;
                case R.id.navigation_notifications:
                    getSupportActionBar().setTitle("Ajustes");
                    getSupportActionBar().setElevation(0);
                    getSupportActionBar().setIcon(R.drawable.ic_menu_send);

                    LibraryFragment fpalpita = new LibraryFragment();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.contentContainer, fpalpita).commit();
                    return true;
            }
            return false;
        }
    };
    FirebaseDatabase database;
    DatabaseReference usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_principal);

        getSupportActionBar().hide();

        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.fundo_degrade));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //getSupportActionBar().setCustomView(R.layout.actionbar);


        Inicio fragmenttab = new Inicio();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.contentContainer, fragmenttab).commit();

        getSupportActionBar().setIcon(R.drawable.nome2);
        database = FirebaseDatabase.getInstance();
        usuario = database.getReference("usuarios");

        //   usuario.child(Global.usuarioAtual.getNomeusuario()).child("cordotime").setValue("vere");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }


}
