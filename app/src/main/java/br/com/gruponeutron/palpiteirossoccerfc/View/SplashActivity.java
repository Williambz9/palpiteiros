package br.com.gruponeutron.palpiteirossoccerfc.View;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.gruponeutron.palpiteirossoccerfc.Global.Global;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Usuario;
import br.com.gruponeutron.palpiteirossoccerfc.R;
import br.com.gruponeutron.palpiteirossoccerfc.View.login.Login;


public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();

    private final int SPLASH_DISPLAY_LENGTH = 3000;
    FirebaseDatabase database;
    DatabaseReference usuario;
    TextView t ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        setStatusBarGradiant(this);

        database = FirebaseDatabase.getInstance();
        usuario = database.getReference("usuarios");

        SharedPreferences sharedPreferences = getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        final String cod = sharedPreferences.getString("login", "");
        final String nome = sharedPreferences.getString("nome", "");
        final String senha = sharedPreferences.getString("senha", "");

        t = (TextView) findViewById(R.id.text) ;
        Typeface te = Typeface.createFromAsset(getAssets(), "fonts/robaga.ttf");
        t.setTypeface(te);



        ActionBar actionBar = getSupportActionBar();
        if(null != actionBar){
            actionBar.hide();
        }

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){

                if (cod.equals("1")) {

                    usuario.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.child(nome).exists()){
                                if (!nome.isEmpty()) {
                                    Usuario login = dataSnapshot.child(nome).getValue(Usuario.class);
                                    if (login.getSenha().equals(senha)) {
                                        startActivity(new Intent(SplashActivity.this, MenuPrincipal.class));
                                        finish();
                                        Global.usuarioAtual = login;

                                    }
                                }}
                                else {
                                startActivity(new Intent(SplashActivity.this, Login.class));
                                finish();


                            }
                        }


                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else {
                    startActivity(new Intent(SplashActivity.this, Login.class));
                    finish();

                }
                                                       }
            }, SPLASH_DISPLAY_LENGTH);


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.fundo_degrade);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }


}
