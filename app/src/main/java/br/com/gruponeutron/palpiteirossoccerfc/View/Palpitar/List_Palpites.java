package br.com.gruponeutron.palpiteirossoccerfc.View.Palpitar;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.gruponeutron.palpiteirossoccerfc.Adapter.JogosViewHolder;
import br.com.gruponeutron.palpiteirossoccerfc.Callback.ItemClickListener;
import br.com.gruponeutron.palpiteirossoccerfc.Model.Jogo;
import br.com.gruponeutron.palpiteirossoccerfc.R;

public class List_Palpites extends AppCompatActivity {

    RecyclerView lista;

    FirebaseRecyclerAdapter<Jogo, JogosViewHolder> adapter;
    LinearLayoutManager layoutManager;
    FirebaseDatabase database;
    DatabaseReference jogos, ligas;
    Button palpitar;
    String[] foraa, cassa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_list__palpites);
        lista = (RecyclerView) findViewById(R.id.list_palpites);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        final TextView carre = (TextView) findViewById(R.id.carre);
        TextView semconx = (TextView) findViewById(R.id.conexao);
        palpitar = (Button) findViewById(R.id.palpitar);

        Intent intent = getIntent();
        Bundle dados = intent.getExtras();
        String serie = dados.getString("cod");


        if (serie.equals("um")){
            palpitar.setVisibility(View.GONE);

        }


        database = FirebaseDatabase.getInstance();
        jogos = database.getReference("Palpites");
        palpitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(List_Palpites.this, UniJogo.class));
                finish();

            }
        });
        layoutManager = new LinearLayoutManager(this);
        lista.setHasFixedSize(true);

        lista.setLayoutManager(layoutManager);


        jogos.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    progressBar.setVisibility(View.GONE);
                    carre.setVisibility(View.GONE);
                    lista.setVisibility(View.VISIBLE);
                    palpitar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);

                    carre.setText("Os jogos ainda não foram publicados, aguarde...");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        adapter = new FirebaseRecyclerAdapter<Jogo, JogosViewHolder>(
                Jogo.class,
                R.layout.item_jogopalpite,
                JogosViewHolder.class,
                jogos.orderByChild("hora")
        ) {

            @Override
            protected void populateViewHolder(final JogosViewHolder viewHolder, final Jogo model, final int position) {

                viewHolder.hora.setText((model.getHora()));
                viewHolder.fora.setText(String.valueOf(model.getFora().substring(0, 3)));
                viewHolder.casa.setText(String.valueOf(model.getCasa().substring(0, 3)));
                viewHolder.comFora.setText(model.getFora());
                viewHolder.compCasa.setText(model.getCasa());
                viewHolder.fin.setText(model.getFase());

                Double jogo = Double.valueOf(1);


                if (!model.getFase().isEmpty()) {
                    viewHolder.classico.setVisibility(View.VISIBLE);
                }

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View ciew, int position, boolean isLongClick) {

                    }
                });


            }
        };


        adapter.notifyDataSetChanged();
        lista.setAdapter(adapter);


    }


}
