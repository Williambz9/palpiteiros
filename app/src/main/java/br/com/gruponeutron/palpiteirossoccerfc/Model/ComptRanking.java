package br.com.gruponeutron.palpiteirossoccerfc.Model;

/**
 * Created by william on 01/02/18.
 */

public class ComptRanking {

    String time;
    int pontos,rodada;

    public ComptRanking(){}
    public ComptRanking(String time, int pontos, int rodada){
        this.pontos = pontos;
        this.rodada = rodada;
        this.time = time;
    }


    public int getPontos() {
        return pontos;
    }

    public int getRodada() {
        return rodada;
    }

    public String getTime() {
        return time;
    }

}
